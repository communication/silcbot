/* The Functions for the silcbot_config interface */

/* Reads the configuration for the Fist time */
void silcbot_config_load(char *filename) {
  config=g_key_file_new();
  g_key_file_load_from_file (config, filename,
			     G_KEY_FILE_KEEP_TRANSLATIONS|G_KEY_FILE_KEEP_COMMENTS, NULL);
}

/* Frees the Configuration */
void silcbot_config_free(){
  g_key_file_free (config);
}

/* Reads a new Configfile */
void silcbot_config_reload(char *filename) {
  silcbot_config_free();
  silcbot_config_load(filename);
}
/* Saves the Config-File */
int silcbot_config_save(char *filename) {
  char *content=g_key_file_to_data(config, NULL, NULL);
  FILE *file=fopen(filename, "w+");
  fprintf(file, "%s", content);
  fclose(file);
}

