/* Functions for The Silcbot Function "Operator Mode" */

int is_operator(SilcClient client,SilcClientConnection conn, 
		SilcChannelEntry channel) {
  SilcClientEntry user;
  SilcChannelUser channel_user;
  user=silc_client_get_client_by_id(client,conn,conn->local_id);
  channel_user=silc_client_on_channel(channel,user);
  if (channel_user->mode & SILC_CHANNEL_UMODE_CHANOP ) 
    return TRUE;
  else
    return FALSE;
}

int is_allowed_ad(SilcClientEntry client) {
  int i=0;
  char **admins;
  if (g_key_file_get_string_list (config,"main", "admins",NULL,NULL)==0)
    return FALSE;
  else
    admins=g_key_file_get_string_list(config,"main", "admins",NULL,NULL);
  while(admins[i]!=0){
    if (!strncmp(client->nickname, admins[i], strlen(admins[i]))){
      free(admins);
      return TRUE;
    }
    i++;
  }
  free(admins);
  return FALSE;
}
int is_allowed_op(char *channel,SilcClientEntry client) {
  int i=0;
  char **ops;
  if (g_key_file_get_string_list (config,channel,"operator",NULL,NULL)==0)
    return FALSE;
  else
    ops=g_key_file_get_string_list(config,channel,"operator",NULL,NULL);
  while(ops[i]!=0){
    if (!strncmp(client->nickname, ops[i], strlen(ops[i]))){
      free(ops);
      return TRUE;
    }
    i++;
  }
  free(ops);
  return FALSE;
}

