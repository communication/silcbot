#ifndef _SILCBOT_OPTIONS
#define _SILCBOT_OPTIONS

/* initialize it */
GKeyFile *config;

/* Defaults */
char default_home[500];
int  default_port;
char default_nick[256];
char default_passwd[500];
char default_host[1000];
char default_join_message[2];
char default_greeting[2];

/* The Version */
char *version="SilcBot by Christian Dietrich 2005 (GPL) - Version 0.03";
#endif
