
#include "history.h"
#include "options.h"

int silcbot_send_histroy_to(SilcClient client, SilcClientConnection conn,
			    SilcClientEntry client_entry,
			    int zahl, const char *channel) {
  char filename[5+strlen(default_home)+strlen(channel)+5];
  sprintf(filename,"%slog.%s",default_home,channel);
  FILE *log;
  int ergebnis=TRUE;
    if (zahl > 0) {
      /* Anzahl der Zeilen bestimmen und l�ngste Zeile herausfinden, 
	 um ein char-Array zu definiere */
      int line=0;
      int long_line=0;
      log=fopen(filename, "r");
      if (!log) { return FALSE; }
      char c='d';
      int i;
      for (i=0; !feof(log); i++) {
	fread(&c, 1,1, log);
	if (!strncmp(&c, "\n",1)) {
	  line=line+1;
	  if (long_line < i) {
	    long_line=i;
	  }
	  i=0;
	}

      }
      long_line++;
      int zeilen=line;
      fclose(log);
      /* Char-Array initalisieren und log einlesen*/
      char lines[line][long_line];
      log=fopen(filename, "r");
      i=0;
      line=0;
      while(!feof(log)) {
	fread(&c, 1,1,log);
	if (strncmp(&c, "\n", 1)) {
	  /* Zeichen hinzuf�gen */

	  lines[line][i]=c;
	  i++;
	}
	else {
	  /* mit \0 terminieren */

	  lines[line][i]=0;
	  line++;
	  i=0;
	}
      }
      fclose(log);
      int begin=(zeilen-zahl)-1;
      zeilen=zeilen-2;
      if (zeilen<0)
	zeilen=0;
      if(begin>zeilen)
	begin=zeilen;
      if (begin < 0)
	begin=0;
      for (; begin <= zeilen; begin++){
	if (strcmp("", lines[begin])) {
	  /*Nachrichten an anfordernden Benutzer schicken*/
	  if (!silc_client_send_private_message(client,conn,client_entry,SILC_MESSAGE_FLAG_UTF8,lines[begin],
						strlen(lines[begin]),TRUE)){
	    ergebnis=FALSE;
	  }

	}
      }
      silc_client_send_private_message(client, conn, client_entry,SILC_MESSAGE_FLAG_UTF8,
				       "----------",strlen("----------"), TRUE);
      return ergebnis;
    }
    else {
      return TRUE;
    }
}
