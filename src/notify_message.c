
/* Notify message to the client. The notify arguments are sent in the
   same order as servers sends them. The arguments are same as received
   from the server except for ID's.  If ID is received application receives
   the corresponding entry to the ID. For example, if Client ID is received
   application receives SilcClientEntry.  Also, if the notify type is
   for channel the channel entry is sent to application (even if server
   does not send it because client library gets the channel entry from
   the Channel ID in the packet's header). */

static void
silc_notify(SilcClient client, SilcClientConnection conn,
	    SilcNotifyType type, ...)
{
  char *str;
  va_list va;
  SilcClientEntry sender, target;
  SilcChannelEntry channel;
  char tmp[300];

  char *join;

  /* Get Nickname */
  char *nickname;
  if (g_key_file_get_string (config,"main", "nickname",NULL)==0)
    nickname=strdup(default_nick);
  else
    nickname=g_key_file_get_string (config,"main", "nickname",NULL);

  va_start(va, type);

  /* Here we can receive all kinds of different data from the server, but
     our simple bot is interested only in receiving the "not-so-important"
     stuff, just for fun. :) */

  switch (type) {
  case SILC_NOTIFY_TYPE_NONE:
    /* Received something that we are just going to dump to screen. */
    str = va_arg(va, char *);
    fprintf(stdout, "--- %s\n", str);
    break;

  case SILC_NOTIFY_TYPE_MOTD:
    /* Received the Message of the Day from the server. */
    str = va_arg(va, char *);
    fprintf(stdout, "%s", str);
    fprintf(stdout, "\n");
    break;

  case SILC_NOTIFY_TYPE_JOIN:
    sender = va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);

    if (g_key_file_get_string (config,channel->channel_name, "join_message",NULL)!=0)
      join=g_key_file_get_string (config,channel->channel_name, "join_message",NULL);  
    else if (g_key_file_get_string (config,"main", "join_message",NULL)!=0)
      join=g_key_file_get_string (config, "main" , "join_message",NULL);  
    else
      join=strdup("");


    /* Oprechte verteilen */
    if (is_operator(client, conn, channel)&& is_allowed_op(channel->channel_name,sender)) {
      sprintf(tmp, "cumode %s +o %s", channel->channel_name, sender->nickname);
      silc_client_command_call(client, conn, tmp);
    }
    if (!strcmp(nickname, sender->nickname)) {
      /* Mich selbst begr�� ich doch nit!*/
      break;
    }
    if (strlen(join)!=0) {
      char text[strlen(join)+strlen(sender->nickname)+1];
      sprintf(text, join, sender->nickname);
      silc_client_send_channel_message(client, conn, channel, channel->curr_key, 
				       SILC_MESSAGE_FLAG_UTF8, text, strlen(text), TRUE);
    }
    free(join);
    break;

  default:
    /* Ignore rest */
    break;
  }

  va_end(va);
  free (nickname);
}


