/* Message for a channel. The `sender' is the sender of the message
   The `channel' is the channel. The `message' is the message.  Note
   that `message' maybe NULL.  The `flags' indicates message flags
   and it is used to determine how the message can be interpreted
   (like it may tell the message is multimedia message). */

static void
silc_channel_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcChannelEntry channel,
		     SilcMessagePayload payload,
		     SilcChannelPrivateKey key,
		     SilcMessageFlags flags, const unsigned char *message,
		     SilcUInt32 message_len)
{
  /* Yay! We got a message from channel. */

  FILE *log;
  char filename[65000]="";
  /* Get Nickname */
  char *nickname;
  if (g_key_file_get_string (config,"main", "nickname",NULL)==0)
    nickname=strdup(default_nick);
  else
    nickname=g_key_file_get_string (config,"main", "nickname",NULL);
  /* Get history */
  int hist=TRUE;
  if (g_key_file_get_value (config,channel->channel_name, "history",NULL)==0) {
    hist=TRUE;
  }
  else {
    hist=g_key_file_get_integer (config,channel->channel_name, "history",NULL);
  }
  

  if (hist==TRUE) {
    sprintf(filename,"%slog.%s",default_home,channel->channel_name);
    log=fopen(filename, "a+");
    fprintf(log, "<%s> %s\n", sender->nickname, message);
    fflush(log);
    fclose(log);
  }

  char pattern[]="^[ ]*history[ ]*([0-9]*) .*";
  size_t regex_nmatch=2;
  regex_t regex;
  regmatch_t regex_pmatch[regex_nmatch];

  char *match=strdup(message);
  int zahl=0;
  if (regcomp(&regex,pattern,REG_EXTENDED)==0&&
      regexec(&regex,match,regex_nmatch,regex_pmatch,0)==0){
    if (zahl=atoi(&match[regex_pmatch[1].rm_so])) {
      silcbot_send_histroy_to(client, conn, sender,
			      zahl, channel->channel_name);
    }

  }
  else if (regcomp(&regex,"^[ ]*hallo(.*)vom.*$",REG_EXTENDED)==0&&
	   regexec(&regex,match,regex_nmatch,regex_pmatch,0)==0){
    /* Begr��ung erwiedern*/
    char b_bot[1000];
    char *name=&match[regex_pmatch[1].rm_so];
    sprintf(b_bot, "^.*%s.*$", nickname);
    if (regcomp(&regex,b_bot,REG_EXTENDED)==0&&
	 regexec(&regex,name,regex_nmatch,regex_pmatch,0)==0) {
       silc_client_send_channel_message(client,conn,channel,
					channel->curr_key,SILC_MESSAGE_FLAG_UTF8,
					"Danke!",strlen("Danke!"),
				       TRUE);
    }
    else {
      char zeile [16+strlen(sender->nickname)];
      sprintf(zeile, "Hallo %s vom Bot", sender->nickname);
      silc_client_send_channel_message(client,conn,channel,channel->curr_key,
				       SILC_MESSAGE_FLAG_UTF8 ,zeile,strlen(zeile),TRUE);
	}
  }
  /* fortune */
  if (!strncmp("fortune", message, 7)) {
    FILE *fortune;
    char *text=NULL, *p, *utf8;
    size_t len = 0;
    fortune=popen("fortune", "r");
    if (fortune == NULL)
      goto free;
    while (getline(&text, &len, fortune) != -1) {
      text[strlen(text)-2]=0;
      if (!LocalToUtf8 (text, &utf8)) continue;
      while((p=strchr(utf8, '\t'))) *p=' ';
      silc_client_send_channel_message(client,conn,channel,channel->curr_key,
				       SILC_MESSAGE_FLAG_UTF8 ,utf8,strlen(utf8),TRUE);
      free(utf8);
    }
    free(text);
    fclose(fortune);
  }
 free:
  regfree(&regex);
  free((char *)match);
  /* Free Configs */
  free (nickname);
}
