/* All needed Includes in one File:*/

#include <silcincludes.h>	/* Mandatory include for SILC applications */
#include <silcclient.h>		/* SILC Client Library API */
#include <regex.h>              /* Regular Expressions */
#include <glib.h>
#include <getopt.h>
#include <time.h>
#include <sys/types.h>           
#include <malloc.h>
#include "options.h"            /* All Options */
#include "history.c"            /* The History Functions */
#include "channel_message.c"    /* Parses what is written in a channel */
#include "private_message.c"    /* Parses what is send via Private Chat*/
#include "notify_message.c"     /* Notify-Messages (Join, Leave, MOTD)*/
#include "silc_connect.c"       /* Is Done after connecting to a server */
#include "silcbot_is.c"         /* Functions is* */
#include "silcbot_config.c"     /* The Config-Interface */
