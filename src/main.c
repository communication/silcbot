/* Main-File */

#include "silcbot.c"



/* The Main-Function */  
int main(int argc, char **argv)
{
  /* Defaults */
  sprintf(default_nick,"pizza");
  sprintf(default_host,"silc.silcnet.org");
  sprintf(default_passwd, "changeme");
  default_port=706;

  const char *help[]={"SilcBot - a little Bot for the SilcNetwork\n"
		"usage: silcbot <options>\n"
		"\noptions:\n"
		"     -c <file>    file is read as configfile\n"
		"     -h           Prints the help message\n"
		"     -v           Prints the version of SilcBot\n",0};

  /* HomeDir */
  sprintf(default_home ,"%s/.silcbot/",getenv("HOME"));
  char configfile[350];
  sprintf(configfile, "%sconfig", default_home);
  printf("%s\n", configfile);
  int c;
  int read_init_file=TRUE;
  /* Read the Commandline */
  while (1) {
    c=getopt(argc, argv, "hvc:");
    if (c==-1) 
      break;
    switch(c) {
    case 'c':
      silcbot_config_load(optarg);
      read_init_file=FALSE;
      break;
    case 'h':
      printf( "%s", help[0]);
      exit (0);
    case 'v':
      printf("%s\n", version);
      break;
    }
    }
  /* Config File einlesen */
  if (read_init_file==TRUE){
    silcbot_config_load(configfile);
  }
  /* Start the bot */
  return mybot_start();
}


