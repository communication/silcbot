/* The Function with is called after the Connect */


void silc_bot_connected( SilcClient client, SilcClientConnection conn) {
  char **channels;
  int i=0, len;
  channels=g_key_file_get_groups (config, NULL);
  len = strlen(channels[i])+6;
  char *tmp=malloc(len);
  if(! tmp) {
    perror("silcbot");
    return;
  }
  while(channels[i]!=NULL ) {
    if (len < (strlen(channels[i]) + 6)) {
      len = strlen(channels[i]) + 6;
      tmp = realloc(tmp, len);
      if(! tmp) {
	perror("silcbot");
	return;
      }
    }
    if (strcmp(channels[i], "main")) {
      sprintf(tmp, "JOIN %s", channels[i]);
      silc_client_command_call(client, conn, tmp); 
    }
    i++;
  }
  free(tmp);
}
  
