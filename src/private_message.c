/* Private message to the client. The `sender' is the sender of the
   message. The message is `message'and maybe NULL.  The `flags'  
   indicates message flags  and it is used to determine how the message
   can be interpreted (like it may tell the message is multimedia
   message). */

static void
silc_private_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcMessagePayload payload,
		     SilcMessageFlags flags,
		     const unsigned char *message,
		     SilcUInt32 message_len)
{


  char *comm=strdup(message);
  /* Überflüssige Leerzeichen entfernen */
  char last_sign=' ';
  int i, act_sign=0;
  for (i=0; i <strlen(message);i++) {
    if (message[i]==last_sign & last_sign==' ')
      continue;
    comm[act_sign]=message[i];
    act_sign++;
    last_sign=message[i];
  }
  comm[act_sign]=0;

  /* Eingabe in Tokens zerlegen */
  char *args[20];
  int argc=0;
  args[0]=strchr(comm, ' ');
  if (args[0]!=NULL) {
    args[0][0]=0;
    argc++;
    args[1]=strchr(&args[0][1], ' ');
    if (args[1]!=NULL) {
      args[1][0]=0;
      argc++;
    }
  }

  /* Get Nickname */
  char *real_passwd;
  if (g_key_file_get_string (config,"main", "passwd",NULL)==0)
    real_passwd=strdup(default_passwd);
  else
    real_passwd=g_key_file_get_string (config,"main", "passwd",NULL);

  /* don't answer noreply messages, some silc clients tend to automatically
     send the set away message right after the message, 'causing endless
     loops without that, etc. */
  if(flags & SILC_MESSAGE_FLAG_NOREPLY) return;
  int zahl=0;
  /* Begin der Auswertung der Usereingabe */
  if (!strcmp(comm, "history")) {
    if (argc <2 )
    if (zahl=atoi(&args[0][1]) && args[1]) {
      char filename[5+strlen(default_home)+strlen(&args[1][1])+5];
      sprintf(filename,"%slog.%s",default_home,&args[1][1]);

      FILE *test=fopen(filename, "r");
      if(test!=0) {
	fclose(test);
	silcbot_send_histroy_to(client, conn, sender,
				zahl, (const char *)&args[1][1]);
	
      }
      else {
	/* Der Channel wird nicht gelogt */
	silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
					 "Dieser Kanal wird nicht gelogt", 
					 strlen("Dieser Kanal wird nicht gelogt"), TRUE);

      }
      
    }

  }
  /****************************************************************\
  \****** HILFE-Funktion ******************************************/
  else if (!strcmp(comm, "help")){
    /* Hier kommt die Hilfefunktion rein;*/
    char *help[]={"Hilfe:",
                  " admins                      Gibt die Bot-Admins aus, wenn du Bot-Admin bist",
		  " channels                    Gibt die vom Bot betreuten Channels aus, wenn du ein Bot-Admin bist",
                  " join <passwd> <channel>     Bot joined den Channel, wenn du Bot-Admin bist",
                  " help                        Gibt die Hilfe aus",
		  " history <lines> <channel>   Gibt die letzen <lines> Zeilen des <channel> aus (Privat)",
		  " leave <passwd> <channel>    Verlaesst den <channel>, wenn <passwd> korrekt ist und du ein Bot-Admin bist",
		  " quit <passwd>               Der Bot beendet sich, wenn <passwd> korrekt ist und du ein Bot-Admin bist",
		  " say <channel> <message>     Der Bot sendet <message> an <channel>, wenn du ein Bot-Admin bist und der Bot <channel> gejoined hat.",
		  " fortune                     Der Bot schickt dir ein kleines Fortune.",
		  " version                     Gibt die akutelle Version aus", 0};
    int i=0;
    for (i=0; help[i]; i++) {
      /* Hilfe an der Anfragenden Schicken */
      silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
				       help[i], strlen(help[i]), TRUE);
    }
  }
  /**************************************************************\
  \****** Version ausgeben *************************************/

  else if (!strcmp(comm, "version")){
    silc_client_send_private_message(client, conn, sender, 
				     SILC_MESSAGE_FLAG_UTF8 , version, 
				     strlen(version), TRUE);

  }
  /**************************************************************\
  \****** Channel-Joinen ****************************************/

  else if (!strcmp(comm, "join")) {
    if (argc < 2) {
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , 
				       "Sorry, falsche Anzahl an Argumenten", 
				       strlen("Sorry, falsche Anzahl an Argumenten"), TRUE);
    }
    else {
      char *passwd=(char *) &args[0][1];
      char *channel=(char *) &args[1][1];

      if (is_allowed_ad(sender)) {
	if(!strncmp(real_passwd, passwd, strlen(real_passwd))) {
	  /* Joinen*/
	  char command[strlen(channel)+20];
	  sprintf(command, "JOIN %s", channel);
	  silc_client_command_call(client, conn, command);
	  char reply[400];
	  sprintf(reply, "Bot hat den Channel %s betreten", channel);
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , reply, 
					   strlen(reply), TRUE);
	  
	}
	else {
	  /* Sry falsches Passwort */
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , "Sorry, das Passwort ist falsch", 
					   strlen("Sorry, dass Passwort ist falsch"), TRUE);
	}
	
      }
      else {
	/* sender ist kein admin */
	silc_client_send_private_message(client, conn, sender, 
					 SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
					 strlen("Sorry, du bist kein Admin"), TRUE);
      }
    }

  }
  /**************************************************************\
  \****** Channel-Leaven ****************************************/

  else if (!strcmp(comm, "leave")) {
    if (argc < 2) {
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , 
				       "Sorry, falsche Anzahl an Argumenten", 
				       strlen("Sorry, falsche Anzahl an Argumenten"), TRUE);
    }
    else {
      char *passwd=(char *) &args[0][1];
      char *channel=(char *) &args[1][1];

      if (is_allowed_ad(sender)) {
	if(!strncmp(real_passwd, passwd, strlen(real_passwd))) {
	  char reason[strlen(sender->nickname)+50];
	  sprintf(reason,"%s hats befohlen", sender->nickname);
	  SilcChannelEntry channel_entry;
	  channel_entry=silc_client_get_channel(client,conn,channel);
	  if (channel_entry!=NULL) {
	    silc_client_send_channel_message(client, conn, channel_entry, channel_entry->curr_key,
					     SILC_MESSAGE_FLAG_UTF8,reason,strlen(reason),
					     TRUE);
	    silc_client_command_call(client, conn, NULL, "LEAVE", channel, NULL);
	    char reply[400];
	    sprintf(reply, "Bot hat den Channel %s verlassen", channel);
	    silc_client_send_private_message(client, conn, sender, 
					     SILC_MESSAGE_FLAG_UTF8 , reply, 
					     strlen(reply), TRUE);
	  }
	  else {
	    silc_client_send_private_message(client, conn, sender, 
					     SILC_MESSAGE_FLAG_UTF8 , 
					     "Entweder den Channel gibt es nicht, oder der Bot hat ihn nicht gejoined", 
					     strlen("Entweder den Channel gibt es nicht, oder der Bot hat ihn nicht gejoined"), TRUE);
	  }
	  
	}
	else {
	  /* Sry falsches Passwort */
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , "Sorry, das Passwort ist falsch", 
					   strlen("Sorry, dass Passwort ist falsch"), TRUE);
	}
	
    }
      else {
	/* sender ist kein admin */
	silc_client_send_private_message(client, conn, sender, 
					 SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
					 strlen("Sorry, du bist kein Admin"), TRUE);
      }
    }
  }
  /**************************************************************\
  \****** Admin-List ********************************************/

  else if (!strcmp(comm, "admins")){
    int i;
    if (is_allowed_ad(sender)) {
      silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
				       "Liste der Admins:",
				       50, TRUE);
      char **admins;
      if (g_key_file_get_string_list (config,"main", "admins",NULL,NULL)==0)
	return;
      else
	admins=g_key_file_get_string_list(config,"main", "admins",NULL,NULL);
      char *send;
      i=0;
      while(admins[i]) {
	send = malloc(strlen(admins[i])+8);
	sprintf(send, " %s", admins[i]);
	silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
					 send,strlen(send), TRUE);
	i++;
	free(send);
      }
    }
    
  
    else {
      /* sender ist kein admin */
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
				       strlen("Sorry, du bist kein Admin"), TRUE);
    }
  }
  /**************************************************************\
  \****** Channel-List ******************************************/

  else if (!strcmp(comm, "channels")){
    if (is_allowed_ad(sender)) {
      SilcChannelEntry channel, channel2;
      SilcHashTableList htl;
      SilcClientEntry user;
      char *send=malloc(1);
      user=silc_client_get_client_by_id(client,conn,conn->local_id);
      silc_hash_table_list(user->channels, &htl);
      silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
				       "Liste der gejoined Channels:",
				       50, TRUE);
      while (silc_hash_table_get(&htl, (void *)&channel, (void *)&channel2)) {
	send=malloc(strlen(channel->channel_name)+8);
	sprintf(send, " %s", channel->channel_name);
	silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
					 send,
					 strlen(send), TRUE);
	free(send);
      }
      silc_hash_table_list_reset(&htl);

    }
    
  
    else {
      /* sender ist kein admin */
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
				       strlen("Sorry, du bist kein Admin"), TRUE);
    }
  }
  /**************************************************************\
  \****** Fortune ***********************************************/
  else if (!strcmp(comm, "fortune")){
    FILE *fortune;
    char *text=NULL, *utf8, *p;
    size_t len = 0;
    fortune=popen("fortune", "r");
    if (fortune != NULL) {
      while (getline(&text, &len, fortune) != -1) {
	text[strlen(text)-2]=0;
	LocalToUtf8 (text, &utf8);
	while((p=strchr(utf8, '\t'))) *p=' ';
	silc_client_send_private_message(client,conn,sender,
					 SILC_MESSAGE_FLAG_UTF8 ,utf8,strlen(utf8),TRUE);
	free(utf8);
      }
    free(text);
    fclose(fortune);
    }
  }
  /**************************************************************\
  \****** Quit **************************************************/

  else if (!strcmp(comm, "quit")) {
    if (argc < 1) {
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , 
				       "Sorry, falsche Anzahl an Argumenten", 
				       strlen("Sorry, falsche Anzahl an Argumenten"), TRUE);
    }
    else {
      char *passwd=(char *) &args[0][1];
      
      if (is_allowed_ad(sender)) {
	if(!strncmp(real_passwd, passwd, strlen(real_passwd))) {
	  char command[]="QUIT";
	  silc_client_command_call(client, conn, command);
	  printf("Beendet von %s\n", sender->nickname);
	  exit(0);
	  
	}
	else {
	  /* Sry falsches Passwort */
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , "Sorry, das Passwort ist falsch", 
					   strlen("Sorry, dass Passwort ist falsch"), TRUE);
	}
	
      }
      else {
	/* sender ist kein admin */
	silc_client_send_private_message(client, conn, sender, 
					 SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
					 strlen("Sorry, du bist kein Admin"), TRUE);
      }
    }
  }
  /****************************************************************\
  \****** Bot sagt was ********************************************/

  else if (!strcmp(comm, "say")){
    if (argc < 2) {
      silc_client_send_private_message(client, conn, sender, 
				       SILC_MESSAGE_FLAG_UTF8 , 
				       "Sorry, falsche Anzahl an Argumenten", 
				       strlen("Sorry, falsche Anzahl an Argumenten"), TRUE);
    }
    else {
      if (is_allowed_ad(sender)) {
	char *channel=(char *) &args[0][1];
	SilcChannelEntry channel_entry;
	char *mesg=strdup(message);
	char *text;
	text=strchr(mesg, ' ');
	text=strchr(&text[1], ' ');
	channel_entry=silc_client_get_channel(client,conn,channel);
	if (channel_entry!=NULL) {
	  silc_client_send_channel_message(client, conn, channel_entry, channel_entry->curr_key,
					   SILC_MESSAGE_FLAG_UTF8,&text[1],strlen(&text[1]),
					   TRUE);
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , 
					   "Nachricht erfolgreich gesendet", 
					   strlen("Nachricht erfolgreich gesendet"), TRUE);      
	}
	else {
	  silc_client_send_private_message(client, conn, sender, 
					   SILC_MESSAGE_FLAG_UTF8 , 
					   "Der Bot ist nicht auf diesem Channel", 
					   strlen("Der Bot ist nicht auf diesem Channel"), TRUE);
	}
	free(mesg);
      }
      else {
	/* sender ist kein admin */
	silc_client_send_private_message(client, conn, sender, 
					 SILC_MESSAGE_FLAG_UTF8 , "Sorry, du bist kein Admin", 
					 strlen("Sorry, du bist kein Admin"), TRUE);
      }
    }
  }
  /**************************************************************\
  \****** Kommando unbekannt*************************************/
  else {
    silc_client_send_private_message(client, conn, sender, SILC_MESSAGE_FLAG_UTF8 , 
			      "Kein solches Kommando bekannt ('help' fuer Hilfe)", 
			      strlen("Kein solches Kommando bekannt ('help' fuer Hilfe)"), TRUE);
  }
  free(comm);
  free(real_passwd);
}
